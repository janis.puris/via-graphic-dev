#1. praktiskais darbs
## Darba uzdevums
Izveidot ar html canvas programmu, kas zīmē līniju izmantojot pikseļa vērtības uzstādīšanu ar Breserhama algoritmu. Praktiskajā darbā var būt tikai vienā oktantā, bet mājasdarbā jāzīmē visos. Lai vieglāk pārbaudītu, izveidot animāciju, kur līnijas viens gals rotē kā pulksteņa rādītājs.

## References
https://www.w3schools.com/tags/canvas_getimagedata.asp
http://www.w3schools.com/tags/ref_canvas.asp
https://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm
