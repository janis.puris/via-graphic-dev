window.onload=function(){
  var canvas = document.getElementById("canvas");
  var ctx = canvas.getContext("2d");
  
  var imgData = ctx.getImageData(0, 0, canvas.width, canvas.height);
  var data = imgData.data;

  var midx = canvas.width / 2

  var lines = [];
  for (var angle = -Math.PI / 2; angle <= Math.PI / 2; angle += Math.PI / 15) {
    lines.push([
      Math.round(-midx * Math.cos(angle)) + midx, Math.round(-midx * Math.sin(angle)) + midx,
      Math.round(midx * Math.cos(angle)) + midx, Math.round(midx * Math.sin(angle)) + midx
    ])
  }

  console.time("drawbresenhamsLineInteger");
  drawbresenhamsLineInteger()
  console.timeEnd("drawbresenhamsLineInteger");

  console.time("drawbresenhamsLineFraction");
  drawbresenhamsLineFraction()
  console.timeEnd("drawbresenhamsLineFraction");

  function drawbresenhamsLineFraction() {
    for (line in lines) {
      bresenhamsLineFraction(lines[line][0], lines[line][1], lines[line][2], lines[line][3], [255, 0, 0, 255]);
    }
  }

  function drawbresenhamsLineInteger() {
    for (line in lines) {
      bresenhamsLineInteger(lines[line][0], lines[line][1], lines[line][2], lines[line][3], [0, 0, 0, 255]);
    }
  }

  // Paint it
  ctx.putImageData(imgData, 0, 0);
  
  function setPixel(x, y, rgba) {
    var n = (y * canvas.width + x) * 4;
    data[n] = rgba[0];
    data[n + 1] = rgba[1];
    data[n + 2] = rgba[2];
    data[n + 3] = rgba[3];
  }

  // https://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm#Method
  function bresenhamsLineFraction(x0, y0, x1, y1, rgba) {
    var deltaX = x1 - x0;
    var deltaY = y1 - y0;
    var error = 0;
    var deltaError;
    
    if (Math.abs(deltaX) >= Math.abs(deltaY)) {
      deltaError = deltaY / deltaX;
      
      if (deltaError < 0) {
        y = y0;
        for (var x = x0; x <= x1; x++) {
          setPixel(x, y, rgba);
          error += deltaError;  
          if (error <= -0.5) {
            y -= 1;
            error += 1;
          }
        }

      } else {
        y = y0;
        for (var x = x0; x <= x1; x++) {
          setPixel(x, y, rgba);
          error += deltaError;  
          if (error > -0.5) {
            y += 1;
            error -= 1;
          }
        }

      }
    } else {
      deltaError = deltaX / deltaY;

      if (deltaError < 0) {
        x = x0;
        for (var y = y0; y >= y1; y--) {
          setPixel(x, y, rgba);
          error += deltaError;
          if (error <= -0.5) {
            x += 1;
            error += 1;
          }
        }

      } else {
        x = x0;
        for (var y = y0; y <= y1; y++) {
          setPixel(x, y, rgba);
          error += deltaError;
          if (error > -0.5) {
            x += 1;
            error -= 1;
          }
        }

      }
    }
  }
  
  // https://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm#All_cases
  function bresenhamsLineInteger(x0, y0, x1, y1, rgba) {

      var deltaX = Math.abs(x1 - x0)
      var deltaY = Math.abs(y1 - y0)

      if (deltaY < deltaX) {
        if (x0 > x1) {
          plotLineLow(x1, y1, x0, y0, rgba);
        } else {
          plotLineLow(x0, y0, x1, y1, rgba);
        }
      } else {
        if (y0 > y1) {
          plotLineHigh(x1, y1, x0, y0, rgba);
        } else {
          plotLineHigh(x0, y0, x1, y1, rgba);
        }
      }

    function plotLineLow(x0, y0, x1, y1, rgba) {
      dx = x1 - x0;
      dy = y1 - y0;
      yi = 1;
      if (dy < 0) {
        yi = -1;
        dy = -dy;
      }
      diff = 2 * dy - dx;
      y = y0;

      for (var x = x0; x < x1; x++) {
        setPixel(x, y, rgba);
        if (diff > 0) {
          y = y + yi;
          diff = diff - 2 * dx;
        }
        diff = diff + 2 * dy;
      }
    }

    function plotLineHigh(x0, y0, x1, y1, rgba) {
      dx = x1 - x0;
      dy = y1 - y0;
      xi = 1;
      if (dx < 0) {
        xi = -1;
        dx = -dx;
      }
      D = 2*dx - dy;
      x = x0;

      for(var y = y0; y < y1; y++) {
        setPixel(x, y, rgba)
        if (D > 0) {
          x = x + xi;
          D = D - 2*dy;
        }
      D = D + 2*dx;
      }
    }
  }
}

